﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class SceneScript : MonoBehaviour {

	public void DoQuitApplication()
	{
		Application.Quit();
	}

	public void DoLoadScene (string sceneName)
	{
		SceneManager.LoadScene (sceneName, LoadSceneMode.Single);
	}

	public void ReloadScene(){
		SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
	}

	public void GoToMainMenu(){
		SceneManager.LoadScene ("Title Menu");
	}
}
