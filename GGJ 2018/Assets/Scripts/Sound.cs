﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class Sound : MonoBehaviour {

	bool soundPanelVisible;
	GameObject soundPanelGO;
	GameObject soundCanvasGO;
	GameObject canvasSound;

	AudioSource bgm;
	AudioSource sfx;
	Slider bgmSlider;
	Slider sfxSlider;
	public AudioClip sfx1;
	public AudioClip sfx2;
	public AudioClip sfx3;

	float sfxDelayTime;
	float sfxTime;
	public float minDelay = 7f;
	public float maxDelay = 20f;

	void Awake()
	{
		
		bgm = GameObject.Find ("BGM_GO").GetComponent<AudioSource>();
		sfx = GameObject.Find ("SFX_GO").GetComponent<AudioSource>();
		bgmSlider = GameObject.Find ("BGMSlider").GetComponent<Slider> ();
		sfxSlider = GameObject.Find ("SFXSlider").GetComponent<Slider> ();

		soundPanelGO = GameObject.Find ("SoundPanel");
		soundPanelGO.SetActive (false);
		soundPanelVisible = false;



		canvasSound = GameObject.Find ("CanvasSound");
		DontDestroyOnLoad (canvasSound);
	}
	// Use this for initialization
	void Start () {
		sfxDelayTime = Random.Range (minDelay, maxDelay);
		sfxTime = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		sfxTime += Time.deltaTime;
		if (sfxTime > sfxDelayTime) {
			PlayRandomSfx ();
			sfxDelayTime = Random.Range (minDelay, maxDelay);
			sfxTime = 0f;
		}
	}

	public void ToggleSoundPanel()
	{
		if (soundPanelVisible) {
			soundPanelGO.SetActive (false);
			soundPanelVisible = false;
		} else {
			soundPanelGO.SetActive (true);
			soundPanelVisible = true;
		}
	}

	public void PlayRandomSfx()
	{
		int i = Random.Range (0, 3);
		switch (i)
		{
		case 0:
			sfx.PlayOneShot (sfx1);
			break;
		case 1:
			sfx.PlayOneShot (sfx2);
			break;
		case 2:
			sfx.PlayOneShot (sfx3);
			break;
		}
	}

	public void ChangeVolBGM ()
	{
		bgm.volume = bgmSlider.value;
	}
	public void ChangeVolSFX ()
	{
		sfx.volume = sfxSlider.value;
	}
}
