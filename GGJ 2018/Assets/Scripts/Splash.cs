﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class Splash : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (DoSplash ());
	}
	
	IEnumerator DoSplash()
	{
		yield return new WaitForSeconds (5f);
		SceneManager.LoadScene ("Title Menu");
	}
}
