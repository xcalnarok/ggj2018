﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Bird : MonoBehaviour
{
	Movement movement;
	public BasePlace currentPlace; // area yang sedang dihinggapi

	public int foodCarriedLimit; // batas mengambil makanan dalam sekali masuk area
	public int[] carriedFood;// berapa macam jenis makanan yang sedang di bawa?
	int totalFoodCarried;


	void Start ()
	{
		movement = GetComponent<Movement> ();
		int foodTypeSize = System.Enum.GetValues (typeof(FoodType)).Length;
		carriedFood = new int[foodTypeSize];// buat array sebanyak jenis makanan
	}

	public bool GoToNextPlace (BasePlace place) // gotonextplace dipanggil pada class BasePlace ketika area di klik. 
	//fungsi ini memeriksa dan melakukan perpindahan burung (animasi, translasi)
	{
		
		if (place.passAble) {
			
			if (currentPlace.adjacentPlace.Contains (place)) {


				moveTo (place.transform.position); // kode sementara, replace dengan animasi yang lebih bagus
				currentPlace.Fall();

				//currentPlace.Occupy ();
				currentPlace = place;
				place.Occupy (); // fungsi ini bisa dipanggil di class bird atau di class baseplace
				return true;
			} else
				return false;

		} else
			return false;

	}

	public void TakeFood(FoodType type){
		if (totalFoodCarried < foodCarriedLimit) {
			carriedFood [(int)type]++;
			totalFoodCarried++;
		}
	}

	public bool GiveFood(FoodType type){
		if (carriedFood [(int)type] > 0) {
			carriedFood [(int)type]--;
			totalFoodCarried--;
			return true;
		}
		return false;
	}

	public bool IsStillMoving(){
		return movement.isJumping;
	}

	void moveTo(Vector3 position){ // TODO : ubah isi ini menjadi animasi yang lebih bagus
		//transform.position = position;
		movement.source = transform.position;
		movement.target = position;
		movement.StartJump ();

	}



}
