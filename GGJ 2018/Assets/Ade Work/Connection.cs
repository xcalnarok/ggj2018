﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connection : ScriptableObject {

	public GameObject branch;
	public BasePlace place;

}
