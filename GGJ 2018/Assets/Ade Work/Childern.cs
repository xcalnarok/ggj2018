﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Childern : MonoBehaviour {

	Animator childrenAnim;
	public FoodType foodRequirment; 
	//public int foodNeed;

	SpriteRenderer rend;

	public bool grownUp{get; private set;}
	//int foodCount;
	Bird bird;

	void Start(){
		rend = GetComponent<SpriteRenderer> ();
		bird = FindObjectOfType<Bird> ();
		childrenAnim = GetComponent<Animator> ();
	}

	public bool Feed(){

		if (grownUp)
			return false;

		if (bird.GiveFood(foodRequirment)) {
			//grownUp = ++foodCount >= foodNeed;

			//if (grownUp)
				doGrownUpEffect ();
				

			return true;
		}
		return false;
	}

	void doGrownUpEffect(){
		grownUp = true;
		StartCoroutine (startAnim ());
	}

	IEnumerator startAnim()
	{
		yield return new WaitForEndOfFrame ();
		while(bird.IsStillMoving())
		{
			yield return null;
		}
		childrenAnim.SetTrigger ("GetFood");
	}
}
