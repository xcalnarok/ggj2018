﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodBank : BasePlace {

	public FoodType foodType;



	public override void Occupy ()
	{
		base.Occupy ();
		bird.TakeFood (foodType);
	}
}
