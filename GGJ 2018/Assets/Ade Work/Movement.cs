﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {
	
	public Vector2 source;
	public Vector2 target;
	public float maxJumpLength;
	public float maxJumpHeight;
	public float moveSpeed;
	public float jumpDelayTime;
	public bool isJumping;

	Animator movAnim;
	Vector2 location;
	Vector2 distanceVector;
	Vector2 jumpVector;
	Vector2 displacement;
	Vector2 altitude;
	int jumpAmount;
	int jumpCounter;
	float startJumpTime;
	float processJumpTime;
	float waittime;
	float jumpLength;
	float jumpPosition;
	bool isWaiting;

	SpriteRenderer r;

	void Start(){
		location = transform.position;
		movAnim = GetComponent<Animator> ();
		r = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space))
		{
			StartJump();
		}
		if(isJumping == true)
		{
			DoJump ();
		}
		transform.position = location;
	}


	public void StartJump()
	{
		distanceVector = target - source;

		r.flipX = distanceVector.x < 0;

		jumpAmount = Mathf.CeilToInt((float)(distanceVector.magnitude / maxJumpLength));
		jumpLength = distanceVector.magnitude / jumpAmount;
		jumpVector = distanceVector / jumpAmount;

		startJumpTime = Time.timeSinceLevelLoad;
		jumpCounter = 0;
		isJumping = true;
		isWaiting = false;
		movAnim.SetBool ("isJumping", true);
		movAnim.SetBool ("isWaiting", false);
	}


		


	void DoJump()
	{
		processJumpTime = Time.timeSinceLevelLoad - startJumpTime;
		jumpPosition = moveSpeed * processJumpTime;

		altitude = Mathf.Sin((float)(Mathf.PI * jumpPosition/jumpLength)) * maxJumpHeight * new Vector2(0,1); // should be in rad
		if (Mathf.Sin(Mathf.PI * jumpPosition/jumpLength) < 0) {
			if(!isWaiting)
			{
				jumpCounter++;
				isWaiting = true;
				movAnim.SetBool ("isWaiting", true);
				waittime = 0;
			}
			//location = source + jumpVector * jumpCounter;
			else if (waittime < jumpDelayTime) {
				waittime += Time.deltaTime;
			} else {
				isWaiting = false;
				movAnim.SetBool ("isWaiting", false);
				//location = source + jumpVector * jumpCounter; //commented?
				startJumpTime = Time.timeSinceLevelLoad;
				if (jumpCounter == jumpAmount) {
					isJumping = false;
					movAnim.SetBool ("isJumping", false);
				}
			}
		} else {
			location = source + jumpVector * jumpCounter + jumpVector * (jumpPosition/jumpLength);
			location += altitude;
		}
	}
}
