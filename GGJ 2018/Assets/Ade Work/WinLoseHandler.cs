﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLoseHandler : MonoBehaviour {

	Childern[] allChildern;
	Bird bird;

	public bool lose,win;
	public GameObject winMessage,loseMessage;
	public Animator anim;



	void Start(){
		win = lose = false;
		allChildern = FindObjectsOfType<Childern> ();
		bird = FindObjectOfType<Bird> ();
	}

	public bool Check(){
		win = true;

		foreach (Childern c in allChildern) {
			win = c.grownUp && win;
			if (!win)
				break;
		}

		if (win) {
			StartCoroutine (showWinLoseMessage ());
		}

		return win;


	}

	public void Lose(){
		Debug.Log ("game over!");
		lose = true;
		StartCoroutine (showWinLoseMessage ());

	}

	public bool isGameOver(){
		return win || lose;
	}

	IEnumerator showWinLoseMessage(){

		while (bird.IsStillMoving ()) {
			yield return null;
		}

		if (lose)
			anim.SetTrigger ("lose");//loseMessage.SetActive (true);
		if(win)
			anim.SetTrigger ("win");//winMessage.SetActive (true);


	}

}
