﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Collider2D))]
public class BasePlace : MonoBehaviour {

	//Class BasePlace adalah baseclass untuk area pada game,
	//pada dasarnya class ini nanti dibagi menjadi 2 subclass yaitu Nest dan Ground

	protected Bird bird;// referensi dari burung induk

	protected SpriteRenderer rend;// refernsi sprite renderer, jika ingin memanipulasi sprite (memberi animasi dsb)
	protected InventoryUI invUI;
	protected WinLoseHandler wlHandler;

	public List<BasePlace> adjacentPlace; // area yang berhubungan dengan area ini
	public List<SpriteRenderer> adjacentBranch;

	public bool isInvinite;
	public int maxOccupy; // berapa kali area ini bisa ditempati sebelum hancur?

	public bool passAble = true; // state yang menyatakan area ini bisa dilewati, dibuat public dengan tujuan jika 
								// suatu saat nanti ingin ditambahkan event dimana area bisa berubah ubah state nya
								// (misal jika ada banjir, area ground tidak bisa dilewati, namun saat banjir selesai, 
								// area bisa dilewati lagi)

	int currentOccupy = 0; // counter yg menghitung berapa banyak area ini telah dilewati

	// Use this for initialization

	void Start () {
		bird = FindObjectOfType<Bird> ();
		rend = GetComponent<SpriteRenderer> ();
		invUI = FindObjectOfType<InventoryUI> ();
		wlHandler = FindObjectOfType<WinLoseHandler> ();

	}

	[ExecuteInEditMode]
	void Update(){

		#if UNITY_EDITOR
		foreach(BasePlace b in adjacentPlace){
			 Debug.DrawLine(transform.position,b.transform.position,Color.red); 
		}
		#endif
	}
		

	void OnMouseDown(){// fungsi dipanggil saat area di klik
		Debug.Log (name + " is clicked");
		if (wlHandler.isGameOver())
			return;
		if (bird.IsStillMoving ())
			return;
		if (bird.GoToNextPlace (this)) { // memanggil fungsi gotonext pada burung. "this" berarti area ini yang akan dituju
			// gotonexplace bertipe boolean agar nanti bisa dikembangkan untuk area ini apa yang terjadi setelah dilewati
			Debug.Log ("moved to "+name);
			//Occupy (); // jika fungsi ini dipanggil disini, maka non aktifkan di class bird begitupula sebaliknya


			invUI.UpdateInventory(); // update inventory
		}
	}

	public virtual void Occupy(){ // fungsi ini dipanggil saat burung berada di area ini
		StartCoroutine(changeNestColor());
	}

	public virtual void Fall(){
		if (!isInvinite) {
			if (currentOccupy++ >= maxOccupy) { // normalnya fungsi ini menghitung berapa kali area telah dilewati dan
				// menonaktifkan ketika melewati batas, fungsi ini bisa di override di subclass
				passAble = false;
		StartCoroutine (beginFalling ());
			}
		}
	}

	IEnumerator changeNestColor(){
		if (!isInvinite) {
			while (bird.IsStillMoving ()) {
				yield return null;
			}

			float gb = (float)(maxOccupy - currentOccupy) / (float)maxOccupy;
			Debug.Log (gb);
			Color c = new Color (1, gb, gb);
			rend.color = c;
		}
	}

	IEnumerator beginFalling(){
		
				//yield return new WaitForEndOfFrame ();
				while (bird.IsStillMoving ()) {
					yield return null;
				}

				//rend.color = Color.black;
				float timer = 0;

		List<Rigidbody2D> fallingBodies = new List<Rigidbody2D> ();

		foreach (SpriteRenderer r in adjacentBranch) {
			fallingBodies.Add(r.GetComponent<Rigidbody2D> ());
		}

		fallingBodies.Add( gameObject.AddComponent<Rigidbody2D> ());

		foreach (Rigidbody2D r in fallingBodies) {
			r.simulated = true;
			r.AddTorque (Random.Range (-500f, 500f));
		}
//		yield return new WaitForSeconds (5);
				while (timer<5f){
					Vector3 pos = transform.position;
					pos.y-=Time.deltaTime * Random.Range(1f,5f);
					transform.position = pos;
//					foreach (SpriteRenderer g in adjacentBranch) {
//						pos = g.transform.position;
//						pos.y-=Time.deltaTime * Random.Range(1f,5f);
//						g.transform.position = pos;
//						//Color newColor = g.color;
//						//newColor.a -= Mathf.Lerp (newColor.a, 0, Time.deltaTime * 5);
//						//g.color = newColor;
//					}
					timer+=Time.deltaTime;
					yield return null;
				}

				gameObject.SetActive (false);
				foreach (SpriteRenderer g in adjacentBranch) {
					g.enabled = false;
				}


		
	}
}


