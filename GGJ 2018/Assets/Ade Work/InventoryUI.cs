﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour {

	public Sprite apple, manggo, banana;
	public Image[] InventoryImage;
	Bird bird;

	void Start(){
		bird = FindObjectOfType<Bird> ();
	}

	public void UpdateInventory(){
		StartCoroutine (waitForBirdAnimationAndUpdate ());
	}

	IEnumerator waitForBirdAnimationAndUpdate(){

		yield return new WaitForEndOfFrame ();

		while (bird.IsStillMoving ()) {
			yield return null;
		}

		int inventoryPositionCount = 0;
		foreach (Image i in InventoryImage) {
			i.enabled = false;
		}

		for (int i = 0; i < bird.carriedFood.Length; i++) {

			for(int y=0;y<bird.carriedFood[i];y++){
				InventoryImage [inventoryPositionCount].enabled = true;
				switch (i) {
				case (int)FoodType.APPLE:
					InventoryImage [inventoryPositionCount++].sprite = apple;
					break;
				case (int)FoodType.MANGGO:
					InventoryImage [inventoryPositionCount++].sprite = manggo;
					break;
				case (int)FoodType.BANANA:
					InventoryImage [inventoryPositionCount++].sprite = banana;
					break;
				}
			}
		}
	}
}

