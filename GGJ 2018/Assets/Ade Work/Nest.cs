﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nest : BasePlace {


	public Childern[] childern; //class Nest memiliki array dari class childern yang merupakan karakter anak burung

	public override void Occupy() // fungsi Occupy di ovveride dengan tambahan mencoba memberi makan anak burung
	{
		base.Occupy ();

		foreach (Childern c in childern) {
			c.Feed ();
		}

		wlHandler.Check ();

	}

	public override void Fall ()
	{
		base.Fall ();
		if (!wlHandler.Check ()) {

			if (!passAble) {
				foreach (Childern c in childern) {
					if (!c.grownUp) {
						wlHandler.Lose ();
						return;
					}
				}
			}
		}
	}



//	void OnMouseDown(){
//		Debug.Log (name + " is clicked");
//		if (bird.GoToNextPlace (this)) {
//			Debug.Log ("moved to "+name);
//		}
//	}

}
